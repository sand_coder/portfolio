Rails.application.routes.draw do

  devise_for :users, path: '', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}
	resources :prt_folios, except: [:show]
	get 'prt_folio/:id', to: 'prt_folios#show', as: 'prt_folio_show'
  get 'angular-item', to: 'prt_folios#angular'

  get 'about-me', to: 'pages#about'
  get 'contact', to: 'pages#contact'

  resources :blogs  do
    member do
      get :toggle_status
    end
  end
  root to: 'pages#home'
	
  # get 'pages/home'
  # get 'pages/about'
  # get 'pages/contact'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
