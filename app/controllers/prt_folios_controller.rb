class PrtFoliosController < ApplicationController
  layout 'prt_folio'
  before_action :set_portfolio_item, only: [:show, :edit, :update, :destroy]
  access all: [:show, :index, :angular], user: {except: [:destroy, :new, :create, :edit, :update]}, site_admin: :all
	def index
		@portfolio_items = PrtFolio.all
	end

  def angular
    @angular_portfolio_item = PrtFolio.angular
  end

	def new	
		@portfolio_item = PrtFolio.new
    3.times { @portfolio_item.technologies.build }
	end

	def create
    @portfolio_item = PrtFolio.new(portfolio_params)

    respond_to do |format|
      if @portfolio_item.save
        format.html { redirect_to prt_folios_path, notice: 'Your portfolio  is live now' }
      else
        format.html { render :new }
      end
    end
  end

  def edit     
  end

  def update
     
    respond_to do |format|
      if @portfolio_item.update(portfolio_params)
        format.html { redirect_to prt_folios_path, notice: 'The record successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def show    
  end

  def destroy
    @portfolio_item.destroy
    respond_to do |format|
      format.html { redirect_to prt_folios_path, notice: 'The record successfully destroyed.' }
    end
  end

  private

  def portfolio_params
    params.require(:prt_folio).permit(:title,  :subtitle, :body,
                                      technologies_attributes: [:name])
  end

  def set_portfolio_item
    @portfolio_item = PrtFolio.find(params[:id])
  end
end
