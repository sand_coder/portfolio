module DefaultPageContent
	extend ActiveSupport::Concern

	included do
		before_action :set_page_defaults 
	end

  def set_page_defaults
  	@page_title = "Portfolio | my portfolio site"
  	@seo_keywords = "Sandeep soni portfolio"
  end

end